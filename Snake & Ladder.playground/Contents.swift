//: Playground - noun: a place where people can play

import UIKit

// Snakes and Ladders Game (OOP)
import Foundation
import UIKit

class Player {
    var name: String
    var position: Int
    
    init(name: String, position: Int) {
        self.name = name
        self.position = position
    }
    
    deinit {
        name = ""
        position = 0
    }
}

let finalSquare = 99
//var players: [String] = ["a", "b", "c", "d", "e"]
var player1: Player = Player(name: "Ahmad", position: 0)
var player2: Player = Player(name: "Chin", position: 0)
var player3: Player = Player(name: "Tamil", position: 0)

var board: [Int] = [Int] (count: finalSquare + 1, repeatedValue: 0)
var players: [Player] = []

players.append(player1)
players.append(player2)
players.append(player3)

board[01] = +37; board[04] = +10; board[09] = +22; board[21] = +21; board[28] = +56; board[51] = +16; board[80] = +20; board[71] = +20;
board[17] = -10; board[62] = -43; board[87] = -63; board[54] = -20; board[64] = -60; board[98] = -19; board[95] = -20; board[93] = -20;

//var numberOfPlayers = //change to array
//var playerPositions: [Int] = [Int](count: numberOfPlayers, repeatedValue: 0)
var diceRoll = 0 // moves are determined by an 8-sided die
var hasWinner: Bool = false

//for (var i = 0; i < playerPositions.count; i++) {
//    print("Game Starting at player postions \(playerPositions[i])")
//    // start the game
//}

while hasWinner == false {
    for player in players {
        
        
        
        
        
        //print("players turn : \(players[i])")
        while player.position < finalSquare {
            
            var diceRoll: Int = Int(random()) % 6
            
            // move by the rolled amount
            player.position += diceRoll
            
            if player.position < board.count {
                
                // if the player is still on the board do what the current square instructs
                let oldSquare = player.position // extra let created for printing the results since square's value has changed
                
                if player.position >= finalSquare{
                    hasWinner = true
                    print(" Winner \(player.name)")
                    break
                } else {
                    player.position += board[player.position]
                    
                    print("Player name = \(player.name) diceRoll = \(diceRoll), landing = \(oldSquare), instruction = \(board[oldSquare]), destination = \(player.position)")
                }
            }
            break
        }
        
        if hasWinner {
            break
        }
    }
}

print("Game over at \(finalSquare + 1)")
